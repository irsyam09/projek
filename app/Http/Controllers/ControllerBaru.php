<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ControllerBaru extends Controller
{
    public function input(){
        return view('input');
    }

    public function proses(Request $request){
        $message = [
            'required' => ':attribute harus isi sebagai kuli yang baik',
            'min' => ':attribute harus diisi minimal :min karakter yaaa kuli',
            'max' => ':attribute harus diisi maksimal :max karakter yaaa kuli'
        ];
        
        $this->validate($request,[
            'nama' => 'required|min:5|max:20',
            'pekerjaan' => 'required',
            'usia' => 'required|numeric'
        ],$message);

        return view('proses',['data' => $request]);
    }
}
