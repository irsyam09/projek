<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;

class IcamController extends Controller
{
    public function index(){
        $encrypted = Crypt::encryptString('Belajar laravel icam nih bos');
        $decrypted = Crypt::decryptString($encrypted);

        echo "Hasil Enkripsi : ". $encrypted;
        echo "<br/>";
        echo "<br/>";
        echo "Hasil Dekripsi : ".$decrypted ;
    }

    public function data(){
		
		$parameter =[
			'nama' => 'Diki Alfarabi Hadi',
            'jenis_kelamin' => 'laki-laki'
            
		];
		$enkripsi= Crypt::encrypt($parameter);
		echo "<a href='/data/".$enkripsi."'>Klik</a>";
	}
 
	public function data_proses($data){
		$data = Crypt::decrypt($data);
 
		echo "Nama : " . $data['nama'];
		echo "<br/>";
        echo "Jenis Kelamin : " . $data['jenis_kelamin'];
	}
}
