<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Pegawais;

class ControllerPegawai extends Controller
{
    public function index(){
        //menampilkan data pegawai
        $pegawais = Pegawais::paginate(10);

        //mengirim data ke view pegawai
        return view ('pegawai', ['pegawais' => $pegawais]);

    }
     
    
}
