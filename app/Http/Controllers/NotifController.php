<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;

class NotifController extends Controller
{
    public function index(){
        return view('notifikasi');
    }

    public function sukses(){
        Session::flash('sukses','selamat kamu sukses');
        return redirect ('pesan');
    }

    public function peringatan(){
        Session::flash('peringatan', 'kamu sedang kuingatkan!');
        return redirect('pesan');
    }

    public function gagal(){
		Session::flash('gagal','Maaf kamu gagal');
		return redirect('pesan');
    }
    
    public function error($nama){
            if($nama == "icammm"){
                return abort(403,'Anda tidak punya akses karena anda gabisa ngoding');
            }elseif($nama == "icam"){
                return "Halo, ".$nama;
            }else{
                return abort(404);
            }
        }
    }

