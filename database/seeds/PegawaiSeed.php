<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class PegawaiSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
            //data faker indonesia
            $faker = Faker::create('id_ID');

            //membuat data sebanyak-banyaknya
            for($x =1; $x <= 15; $x++){
                DB::table('pegawais')->insert([
                    'nama' => $faker->name,
                    'alamat' => $faker->address
                ]);
            }
    }
}
