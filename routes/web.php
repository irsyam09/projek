<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
	return view('welcome');
});
 
Route::get('/halo', function () {
	return "Halo, Selamat datang di laravel";
});
 
Route::get('/blog', function () {
	return view('blog');
});

Route::get('tiki', 'tiki@index');

Route::get('/testing/{nama}', 'test@index');

Route::get('/formulir', 'test@formulir');
Route::post('/formulir/proses', 'test@proses');

Route::get('/pegawai','PegawaiController@index');
Route::get('/pegawai/cari', 'PegawaiController@cari');
Route::get('/pegawai/tambah','PegawaiController@tambah');
Route::post('/pegawai/proses','PegawaiController@proses');
Route::get('/pegawai/edit/{id}','PegawaiController@edit');
Route::post('/pegawai/update','PegawaiController@update');
Route::get('/pegawai/hapus/{id}','PegawaiController@hapus');

Route::get('/input', 'ControllerBaru@input');
Route::post('/proses', 'ControllerBaru@proses');

Route::get('/pegawaibaru', 'ControllerPegawai@index');

Route::get('/enkripsi', 'IcamController@index');
Route::get('/data', 'IcamController@data');
Route::get('/data/{data_rahasia}', 'IcamController@data_proses');

Route::get('/pesan', 'NotifController@index');
Route::get('/pesan/sukses', 'NotifController@sukses');
Route::get('/pesan/peringatan', 'NotifController@peringatan');
Route::get('/pesan/gagal' , 'NotifController@gagal');

Route::get('/icam/{nama}','NotifController@error');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
