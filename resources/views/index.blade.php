<html>
    <head>
        <title>Tutorial Pegawai Icam lah yaa</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/app.css') }}">
    </head>
    <body>

        <style type="text/css">
            .pagination li{
                float: left;
                list-style-tipe: none;
                margin: 5px;
            }
        </style>

        <h2>Icam ICAm ICHAMMM</h2>
        <h3>Data Pegawai</h3>
        
        <p>Cari data kuli-kuli</p>
        <form action="/pegawai/cari" method="get">
        <input type="text" name="cari" placeholder="carilah kuli kesukaan anda ....." value="{{ old('cari') }}">
        <input type="submit" value="cari">
        </form>
        <br/>

        <a href="/pegawai/tambah"> + Tambah Kuli baru broo</a>

        <br/>
        <br/>

        <table border="1">
            <tr>
                <th>Nama</th>
                <th>Jabatan</th>
                <th>Umur</th>
                <th>Alamat</th>
                <th>Opsi</th>
            </tr>
            @foreach ($pegawai as $p)
            <tr>
                <td>{{ $p->pegawai_nama }}</td>
                <td>{{ $p->pegawai_jabatan}}</td>
                <td>{{ $p->pegawai_umur }}</td>
                <td>{{ $p->pegawai_alamat}}</td>
                <td>
                    <a href="/pegawai/edit/{{ $p->pegawai_id}}">Edit boy ||</a>
                    <a href="/pegawai/hapus/{{ $p->pegawai_id}}"> Hapus nih </a>
                </td>
            </tr>   
            @endforeach

        </table>

        <br/>
        Halaman : {{ $pegawai->currentPage() }} <br/>
        Jumlah Data : {{ $pegawai->total() }} <br/>
        Data Per Halaman {{ $pegawai->perPage() }} <br/>

        {{ $pegawai->links() }}

    </body>
</html>