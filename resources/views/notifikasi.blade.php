<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Notifikasi</title>
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
    <div class="container mt-5">
        <div class="row">
            <div class="col-md-12">
                <center>
                    <h2>Ini Latihan bikin notifikasi</h2>
                    <h4><a href="#">WWW.inilahicam.com</a></h4>
                </center>

                @if ($message = Session::get('sukses'))
                    <div class="alert alert-success alert-block">
                        <button class="close" type="button" data-dismiss="alert">x</button>
                        <strong>{{ $message }}</strong>
                    </div>
                @endif

                @if ($message = Session::get('peringatan'))
                    <div class="alert alert-warning alert-block">
                        <button type="button" class="close" data-dismiss="alert">x</button>
                        <strong>{{ $message }}</strong>
                    </div>
                @endif

                @if ($message = Session::get('gagal'))
                    <div class="alert alert-danger alert-block">
                        <button class="close" type="button" data-dismiss="alert">x</button>
                        <strong>{{ $message }}</strong>
                    </div>
                @endif

                <center>
                    <a href="/pesan/sukses" class="btn btn-success">Yeay kamu berhasil</a>
                    <a href="/pesan/peringatan" class="btn btn-warning">Awas, Hati-hati</a>
                    <a href="/pesan/gagal" class="btn btn-danger">Maaf sekali, anda gagal</a>
                </center>

            </div>
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>
</html>